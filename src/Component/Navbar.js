import React from 'react';
import { Link } from 'react-router-dom';

function Navbar() {

  const register = () => {

  }

  return (
    <div className="flex justify-between items-center p-5 ">
      <div className="">
        <h3 className='text-[40px] text-amber-900'><span className='text-[60px]'>F</span>ashion <span className='text-[60px]'>S</span>hopee</h3>
      </div>
      <ul className="flex space-x-5 text-xl mt-2">
        <li><Link to="/">
          Home</Link></li>
        <li><Link to="/Product">
          Product</Link></li>
        <li><Link to="/About">
          About</Link></li>
        <li><Link to="/Contact">
          Contact</Link></li>
      </ul>
      <div className="flex space-x-4">
        <Link to="/login"><button className='border-2 border-gray-400 rounded-lg p-2'><i className="fa-solid fa-arrow-right-to-bracket"></i> Login</button></Link>
        <Link to="/signup"><button className='border-2 border-gray-400 rounded-lg p-2' onClick={() => register}><i className="fa-solid fa-user-plus"></i> Register</button></Link>
        <button className='border-2 border-gray-400 rounded-lg p-2'><i className="fa-solid fa-cart-shopping"></i>Cart</button>
      </div>
    </div>
  );
}
export default Navbar;
