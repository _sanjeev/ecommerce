import React from 'react';
import { useParams } from 'react-router-dom';
import { useState, useEffect } from 'react';
import Navbar from './Navbar';

export default function ProductDetails(props) {
    const params = useParams()
    // console.log(params.id);
    console.log(props);

    const [product, setProduct] = useState([]);

    const findProduct = () => {
        fetch('https://fakestoreapi.com/products/' + params.id)
            .then((r) => r.json())
            .then((data) => {
                setProduct(data);
            });
    };
    useEffect(() => {
        findProduct();
    }, []);

    const addProduct = () => {
        props.addToCart (params.id);
    }

    return (
        <>
            <Navbar />
            <div className='flex justify-center items-center mt-10'>
                <div>
                    <img src={product.image} alt="product Image" className='w-[500px] h-[500px]' />
                </div>
                <div className='flex flex-col justify-between pl-28 w-[50%] h-[500px]'>
                    <h1 className='uppercase text-gray-600 text-3xl'>{product.category}</h1>
                    <h1 className='text-[45px] text-gray-700'>{product.title}</h1>
                    <p className='font-semibold text-lg'>Rating {product?.rating?.rate} <i class="fa fa-star"></i></p>
                    <p className='text-2xl '>{product.description}</p>
                    <p className='font-bold text-[40px]'>$ {product.price}</p>
                    <div className='flex'>
                        <button className='border-2 border-gray-400 p-4 mr-5 rounded-lg' onClick={addProduct}>Add to Cart</button>
                        <button className='border-2 border-gray-400 p-4 rounded-lg'>Buy Now</button>
                    </div>
                </div>
            </div>
        </>
    )
}
