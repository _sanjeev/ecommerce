import React from 'react';
import Navbar from './Navbar';
import Photo from './Photo';
import Product from './Product';


export default function Home() {
    return (
        <div>
            <Navbar />
            <Photo />
            <Product />
        </div>
    )
}
