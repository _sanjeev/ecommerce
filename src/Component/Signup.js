import React, { useState } from 'react'

export default function Signup() {
    const [formData, setFormData] = useState ([]);
    const handleSubmit = (data) => {
        
    }
    return (
        <div className='bg-[#d0cfcf] h-[100vh] '>
            <div className='flex justify-center'>
                <div className='flex flex-col items-center shadow-lg w-[500px] space-y-10 bg-white p-3 mt-10 justify-center'>
                    <div>
                        <h1 className='text-3xl'>SignUp</h1>
                    </div>
                    <div className='flex flex-col items-center space-y-3'>
                        <label>Enter your name</label>
                        <input type="text" placeholder='Enter your name' className='border-2 border-gray-400 w-[400px] p-1' />
                    </div>
                    <div className='flex flex-col items-center space-y-3'>
                        <label>Enter your email</label>
                        <input type="email" placeholder='Enter your email' className='border-2 border-gray-400 w-[400px] p-1' />
                    </div>
                    <div className='flex flex-col items-center space-y-3'>
                        <label>Enter your number</label>
                        <input type="number" placeholder='Enter your number' className='border-2 border-gray-400 w-[400px] p-1' />
                    </div>
                    <div className='flex flex-col items-center space-y-3'>
                        <label>Enter your Age</label>
                        <input type="number" placeholder='Enter your age' className='border-2 border-gray-400 w-[400px] p-1' />
                    </div>
                    <div className='flex flex-col items-center space-y-3'>
                        <label>Enter your password</label>
                        <input type="password" placeholder='Enter your password' className='border-2 border-gray-400 w-[400px] p-1' />
                    </div>
                    <div className='flex flex-col items-center space-y-3'>
                        <label>Enter your confirm password</label>
                        <input type="password" placeholder='ReEnter your password' className='border-2 border-gray-400 w-[400px] p-1' />
                    </div>
                    <div className='flex justify-start items-center'>
                        <input type="checkbox" />
                        <label>Agree the terms and conditions</label>
                    </div>
                    <div className='bg-pink-600 px-8 py-4 rounded-lg'>
                        <input type="submit" value="Register" className='text-white text-xl'/>
                    </div>
                </div>
            </div>
        </div>
    )
}
