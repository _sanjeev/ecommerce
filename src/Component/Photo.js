import React from 'react';

import photo1 from '../Image/shopping1.jpg';
import photo2 from '../Image/shopping2.jpg';
import photo3 from '../Image/shopping3.jpg';
let arr = [photo1, photo2, photo3];

function Photo() {
  return (
    <div className="Photo">
      <img src={photo1} style={{width:"100%"}} alt=''/>
    </div>
  );
}
export default Photo;
