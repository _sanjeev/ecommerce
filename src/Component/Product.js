import React from 'react';
import { useEffect, useState } from 'react';
import {Link} from 'react-router-dom';
import Footer from './Footer';

function Product() {
  const [product, setProduct] = useState([]);
  const [result, setResult] = useState([]);

  const findProduct = () => {
    fetch('https://fakestoreapi.com/products')
      .then((r) => r.json())
      .then((data) => {
        setProduct(data);
        setResult(data);
      });
  };
  useEffect(() => {
    findProduct();
  }, []);

  const mensCollections = () => {
    const res = product.filter((key) => {
      return key.category === "men's clothing"
    });
    setResult(res);
  }

  const womensCollections = () => {
    const res = product.filter((key) => {
      return key.category === "women's clothing"
    });
    setResult(res);
  }

  const jewelleryCollections = () => {
    const res = product.filter((key) => {
      return key.category === "jewelery"
    });
    setResult(res);
  }

  const electronicsCollections = () => {
    const res = product.filter((key) => {
      return key.category === "electronics"
    });
    setResult(res);
  }

  const allCategory = () => {
    setResult(product);
  }

  

  return (

    <div>
      {console.log("pppppppppp", result)}
      <h1 className='text-3xl text-center mb-10  m-5'>New Collection</h1>
      <p className='border-2 border-gray-400 mt-5 mb-8 mx-16'/>

      <div className='flex justify-center space-x-4 mb-10'>
        <button className='border-2 border-gray-400 rounded-lg p-2' onClick={allCategory}>All</button>
        <button className='border-2 border-gray-400 rounded-lg p-2' onClick={mensCollections}>Men's Clothing</button>
        <button className='border-2 border-gray-400 rounded-lg p-2' onClick={womensCollections}>Women's Clothing</button>
        <button className='border-2 border-gray-400 rounded-lg p-2' onClick={jewelleryCollections}>Jewellery</button>
        <button className='border-2 border-gray-400 rounded-lg p-2' onClick={electronicsCollections}>Electronics</button>
      </div>

      <div className="flex flex-wrap justify-center">
        {result.map((ele) => (
          <div className='w-[400px] shadow-lg m-3 rounded-lg border-2 border-[#D1D0CE] cursor-pointer'>
            <Link to={{pathname: `/productdetails/${ele.id}`}}>
              <img src={ele.image} alt="" className="w-[100%] h-[500px] rounded-lg" />
              <h1 className="text-xl text-center">{ele.title}</h1>
              <p className='text-xl text-center'>${ele.price}</p>
            </Link>
          </div>
        ))}
      </div>

      <Footer />
    </div>
  );
}
export default Product;
