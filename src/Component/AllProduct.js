import React from 'react'
import Navbar from './Navbar'
import { useEffect, useState } from 'react';
import {Link} from 'react-router-dom';
import Footer from './Footer';

export default function AllProduct() {
    const [product, setProduct] = useState([]);
  const [result, setResult] = useState([]);

  const findProduct = () => {
    fetch('https://fakestoreapi.com/products')
      .then((r) => r.json())
      .then((data) => {
        setProduct(data);
        setResult(data);
      });
  };
  useEffect(() => {
    findProduct();
  }, []);
  return (

    <div>
        <Navbar />
        <div className="flex flex-wrap justify-between">
        {result.map((ele) => (
          <div className='w-[400px] shadow-lg m-3 rounded-lg border-2 border-[#D1D0CE] cursor-pointer'>
            <Link to={{pathname: `/productdetails/${ele.id}`}}>
              <img src={ele.image} alt="" className="w-[100%] h-[500px] rounded-lg" />
              <h1 className="text-xl text-center">{ele.title}</h1>
              <p className='text-xl text-center'>${ele.price}</p>
            </Link>
          </div>
        ))}
      </div>

      <Footer />

    </div>
  )
}
