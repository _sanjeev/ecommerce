import { useEffect, useState } from 'react';
import React from "react";
import Navbar from "./Component/Navbar";
import Photo from "./Component/Photo";
import Product from "./Component/Product";
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from "./Component/Home";
import ProductDetails from "./Component/ProductDetails";
import AllProduct from "./Component/AllProduct";
import Signup from './Component/Signup';
import Login from './Component/Login';





function App(props) {
  const [product, setProduct] = useState([]);
  const [addToCartProduct, setaddToCartProduct] = useState([]);
  const findProduct = () => {
    fetch('https://fakestoreapi.com/products')
      .then((r) => r.json())
      .then((data) => {
        setProduct(data);
      });
  };
  useEffect(() => {
    findProduct();
  }, []);
  
  return (
    
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/productdetails/:id" component={() => <ProductDetails/>}/>
        <Route exact path="/Product" component={AllProduct} />
        <Route exact path="/signup" component={Signup} />
        <Route exact path="/login" component={Login} />
      </Switch>
  );
}

export default App;
