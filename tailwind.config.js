module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}"
  ],
  theme: {
    extend: {
      backgroundImage: {
        'hero-pattern': "url('./Image/shopping1.jpg')",
        'footer-texture': "url('./Image/shopping2.jpg')",
      }
    }
  },
  plugins: [],
}
